/* 
 *   Copyright 2013 HUBzero Foundation, LLC
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

module.exports = function(conf) {
	var hg = require('hubgraph'),
		fs = require('fs')
		;

	hg.log = console.log;

	if (!conf) {
		conf = '/etc/hubgraph.conf';
	}
	// load conf file from path
	if (typeof conf == 'string') {
		if (!fs.existsSync(conf)) {
			hg.log('configuration file ' + conf + ' not found');
		}
		else {
			hg.conf = require('iniparser').parseSync(conf);
		}
	}
	// or just use a native structure
	else {
		hg.conf = conf;
	}

	// redirect log messages
	if (hg.conf.hubgraph && hg.conf.hubgraph.log) {
		var logFile = fs.openSync(hg.conf.hubgraph.log, 'w+');
		hg.log = function(data) {
			if (typeof(data) !== 'string') {
				data = JSON.stringify(data);
			}
			fs.writeSync(logFile, data + "\n");
		};
	}
	
	hg.event = {
		'observers': {},
		/**
		 * Register a callback for an event
		 * @param evt the event name
		 * @param callback 
		 * @return null
		 */
		'observe': function(evt, callback) {
			if (!hg.event.observers[evt]) {
				hg.event.observers[evt] = [];
			}
			hg.event.observers[evt].push(callback);
		},

		/**
		 * Invoke observers for an event with the given arguments
		 * @param evt the event name
		 * @param args (optional) array of arguments to pass along
		 * @return array of observer responses
		 */
		'emit': function(evt, args) {
			hg.log(evt);
			if (!args) {
				args = [];
			}
			if (!hg.event.observers[evt]) {
				return;
			}
			var rv = [];
			for (var idx = 0; idx < hg.event.observers[evt].length; ++idx) {
				rv.push(hg.event.observers[evt][idx].apply(null, args));
			}
			return rv;
		}
	};

	// deep copy of an object. used in search a time or two to create two paths for a set of results, eg. one counted for totals and one truncated for the visible page
	hg.clone = function(obj) {
		if (obj == null || typeof(obj) != 'object') {
			return obj;
		}
		
		var rv = obj.constructor();
		for(var key in obj) {
			rv[key] = hg.clone(obj[key]);
		}
		return rv;
	};

	/**
	 * Break a string into a list of stems, then follow them out to associated vertices
	 */
	hg.selectText = function(str) {
		var  rv = hg.empty(),
		  stems = hg.splitStems(str);
		if (!stems.length) {
			return rv;
		}
		stems.forEach(function(stem) {
			rv.union(hg.select('aliases', stem));
		});
		return rv.adjustWeights(function(item) {
			item.words = item.id.split(/\W+/).length;
			return item.weight/stems.length * (1 - (item.words - item.weight)/item.words);
		}).out('alias', 'text');
	};

	hg.reload = function() {
		hg.log('reload from ' + hg.conf.hubgraph.root + '/data');
		return hg.load(hg.conf.hubgraph.root + '/data');
	};

	hg.checkpoint = function() {
		return hg.save(hg.conf.hubgraph.root + '/data');
	};

	hg.exit = function(code) {
		hg.event.emit('onStop');
		process.exit(code || 0);
	};

	hg.requireJsExtension = function(folder, args) {
		var viewPath = hg.conf.hubgraph.root + '/lib/' + folder + '/',
		        path = require('path'),
		       files = fs.readdirSync(viewPath).filter(function(file) { return path.extname(file) == '.js'; }).sort(),
		       count = files.length,
		eventBase = 'onExtension' + folder[0].toUpperCase() + folder.substr(1).replace(/\/./g, function(x) { return x.replace('/', '').toUpperCase(); });
		hg.log(viewPath);
		// clear node's cache, we want to reload things without restarting the process
		for (var k in require.cache) {
			if (require.cache[k].id.indexOf(viewPath) == 0) {
				delete require.cache[k];
			}
		}
		files.forEach(function(file) {
			hg.log(viewPath + file);
			require(viewPath + file)(hg, function() {
				if (--count == 0) {
					hg.event.emit(eventBase + 'Loaded', args ? args : []);
				}
			});
		});
		hg.event.emit(eventBase + 'Started', args ? args : []);
	};

	hg.split = function(val, sep) {
		if (typeof val == 'object' && val instanceof Array) {
			return val;
		}
		if (val === null || val === undefined) {
			return [];
		}
		if (val.split) {
			return val.split(sep ? sep : '\n');
		}
		return [val];
	};

	hg.Selector.prototype.limit = function(num) {
		this.items = this.items.slice(0, num);
		return this;
	};

	hg.Selector.prototype.union = function(other, doSort, isNew) {
		if (!this.items.length && isNew) {
			this.items = other.items;
			return this;
		}
		var map = {};
		this.items.forEach(function(item, idx) {
			map[item.domain + ':' + item.id] = item;
		});
		other.items.forEach(function(item) {
			var qid = item.domain + ':' + item.id;
			if (map[qid] && map[qid].weight) {
				map[qid].weight += item.weight;
			}
			else {
				map[qid] = item;
			}
		});

		var newItems = [];
		for (var k in map) {
			newItems.push(map[k]);
		}
		this.items = newItems;
		if (doSort === undefined || doSort) {
			this.sort(); 
		}
		return this;
	};
	
	hg.Selector.prototype.intersect = function(other, doSort) {
		var newItems = [];
		var map = {};
		other.items.forEach(function(item) {
			map[item.domain + ':' + item.id] = item.weight;
		});
		this.items.forEach(function(item) {
			if (map[item.domain + ':' + item.id]) {
				item.weight += map[item.domain + ':' + item.id];
				newItems.push(item);
			}
		});
		this.items = newItems;
		if (doSort === undefined || doSort) {
			this.sort(); 
		}
		return this;
	};

	hg.Selector.prototype.resetWeights = function() {
		this.items.forEach(function(item) {
			item.weight = 1;
		});
		return this;
	};

	hg.Selector.prototype.normalizeWeights = function() {
		if (this.items[0]) {
			var max = this.items[0].weight;
			this.items.forEach(function(item) {
				item.weight /= max;
			});
		}
		return this;
	};

	hg.Selector.prototype.scaleWeights = function(factor) {
		var doSort = typeof factor == 'function';
		this.items.forEach(function(item) {
			item.weight *= doSort ? factor(item) : factor;
		});
		if (doSort) {
			this.sort();
		}
		return this;
	};

	hg.Selector.prototype.adjustWeights = function(callback, doSort) {
		this.items.forEach(function(item) {
			item.weight = callback(item);
		});
		if (doSort === undefined || doSort) {
			this.sort(); 
		}
		return this;
	};

	hg.Selector.prototype.eachFollow = function(type, dir, doSort) {
		var newItems = [];
		this.items.forEach(function(item) {
			var followed = hg.select(item.domain, item.id)[dir](type);
			if (followed.items[0]) {
				newItems.push(followed.items[0]);
			}
		});
		this.items = newItems;
		if (doSort === undefined || doSort) {
			this.sort(); 
		}
		return this;
	};

	hg.Selector.prototype.eachOut = function(type, doSort) {
		return this.eachFollow(type, 'out', doSort);
	};
	
	hg.Selector.prototype.eachIn = function(type, doSort) {
		return this.eachFollow(type, 'in', doSort);
	};

	hg.Selector.prototype.sort = function(callback) {
		if (!callback) {
			callback = function(a, b) {
				return a.weight == b.weight ? 0 : a.weight > b.weight ? -1 : 1;
			};
		}
		this.items = this.items.sort(callback);
		return this;
	};

	hg.Selector.prototype.filter = function(callback) {
		this.items = this.items.filter(callback);
		return this;
	};

	hg.Selector.prototype.clone = function() {
		var rv = hg.empty();
		rv.items = this.items;
		return rv;
	};
	return hg;
};
